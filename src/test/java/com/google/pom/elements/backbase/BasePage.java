package com.google.pom.elements.backbase;

import org.openqa.selenium.By;

import com.google.library.functions.CommonDef;
import com.google.framework.Locators;

public class BasePage {
	public static By searchTxt() {
		return CommonDef.locatorValue(Locators.XPATH, "//input[@name='q']");
	}

	public static By searchBtn() {
		return CommonDef.locatorValue(Locators.XPATH, "(//*[@class='FPdoLc VlcLAe']/center/input)[1]");
	}

}
