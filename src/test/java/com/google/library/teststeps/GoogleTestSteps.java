package com.google.library.teststeps;
import org.openqa.selenium.WebDriver;
import com.relevantcodes.extentreports.LogStatus;
import com.google.Driver.DriverFactory;
import com.google.GalenFramework.GalenMethods;
import com.google.PageObject.HomePage;
import com.google.framework.ConfigProvider;
import com.google.framework.ExReporter;
import com.google.framework.ProjectConfig;
import com.google.framework.TestData;
import com.google.library.functions.CommonDef;
import com.google.library.functions.MethodDef;

public class GoogleTestSteps {
	public static String Browser = ConfigProvider.getConfig("Browser").toUpperCase();

	public GoogleTestSteps() {
		initReport();
		logDetails();
		DriverFactory.driverInit();
	}
	public static void initReport() {
		try {
			String testName = ConfigProvider.getConfig("Testname") + "-" + TestData.getConfig("DataBinding");
			String desc = "";
			if (ProjectConfig.getPropertyValue("versionspecific").equals("true")
					|| ConfigProvider.getConfig("Platform").contains("BrowserStack"))
				desc = ConfigProvider.getConfig("Browser") + "-" + ConfigProvider.getConfig("Version");
			else
				desc = ConfigProvider.getConfig("Browser");
			ExReporter.initTest(testName, desc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	// System.out.println(ProjectConfig.getPropertyValue("url"));
	private void logDetails() {
		try {
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Browser"))
				ExReporter.log(LogStatus.INFO, "URL: " + ProjectConfig.getPropertyValue("url"));
			else
				ExReporter.log(LogStatus.INFO, "URL: " + ProjectConfig.getPropertyValue("murl"));
			ExReporter.log(LogStatus.INFO, "OS: " + ConfigProvider.getConfig("OS"));
			ExReporter.assignCatogory(ConfigProvider.getConfig("OS"));
			ExReporter.assignCatogory(ConfigProvider.getConfig("Testname"));
			// ExReporter.assignCatogory(ConfigProvider.getConfig("Testname")+"
			// => "+ConfigProvider.getConfig("EPIC"));
			if (ConfigProvider.getConfig("EPIC") != null) {
				ExReporter.assignCatogory(ConfigProvider.getConfig("EPIC"));
			}
			// ExReporter.assignCatogory(ConfigProvider.getConfig("Testname"));
			/**
			 * if
			 * (ProjectConfig.getPropertyValue("versionspecific").equals("true")
			 * 
			 * || ConfigProvider.getConfig("Platform").contains("BrowserStack"))
			 * {
			 * 
			 * ExReporter.log(LogStatus.INFO,
			 * 
			 * "Browser: " + ConfigProvider.getConfig("Browser") + "-" +
			 * ConfigProvider.getConfig("Version"));
			 * 
			 * ExReporter.assignCatogory(ConfigProvider.getConfig("Browser") +
			 * "-" + ConfigProvider.getConfig("Version"));
			 * 
			 * } else {
			 * 
			 * ExReporter.log(LogStatus.INFO, "Browser: " +
			 * ConfigProvider.getConfig("Browser"));
			 * 
			 * ExReporter.assignCatogory(ConfigProvider.getConfig("Browser"));
			 * 
			 * }
			 */
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void sampleGoogleCompatibility(String page_name) {
		MethodDef.launchApplication();
		GalenMethods.galenspec("homePage", "homePage", "homePage");
		HomePage.searchForSite();
		GalenMethods.galenspec("searchPage", "searchPage", "searchPage");
		
	}
}
