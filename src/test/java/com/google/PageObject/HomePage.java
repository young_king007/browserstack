package com.google.PageObject;

import com.google.library.functions.CommonDef;
import com.google.pom.elements.backbase.BasePage;

public class HomePage extends BasePage{

	public static void searchForSite() {
		CommonDef.sendKeys(searchTxt(), "standard chartered bank");
		CommonDef.findElement(searchTxt()).submit();
		CommonDef.waitForPageLoad();
	}
}
