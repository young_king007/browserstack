package com.google.Driver;

import org.openqa.selenium.WebDriver;

import com.google.framework.ConfigProvider;


public class DriverFactory {
	private static ThreadLocal<WebDriver> currentDriver = new ThreadLocal<WebDriver>();

	public static WebDriver getCurrentDriver() {
		WebDriver driver = currentDriver.get();
		if (driver != null) {
			return driver;
		} else {
			return null;
		}
	}

	public static void driverInit() {
		String browser = ConfigProvider.getConfig("Platform").toUpperCase();
		if(!browser.contains("BROWSERSTACK"))
			browser=ConfigProvider.getConfig("Browser").toUpperCase();		
		//System.out.println(browser);
		switch (browser) {
		case "FIREFOX":
			currentDriver.set(new DesktopDriver().getNewDriver());
			break;
		case "CHROME":
			currentDriver.set(new DesktopDriver().getNewDriver());
			break;
		case "IE":
			//System.out.println("inside ie");
			currentDriver.set(new DesktopDriver().getNewDriver());
			break;
		case "SAFARI":
			currentDriver.set(new DesktopDriver().getNewDriver());
			break;
		case "EDGE":
			currentDriver.set(new DesktopDriver().getNewDriver());
			break;
		default:
			//System.out.println("Unknown Driver");
		}
		
	}

	public static void closeDriver() {
		
		try {
			WebDriver driver = currentDriver.get();
			if (driver != null) {
				//getCurrentDriver().close();
				getCurrentDriver().quit();
			}
			currentDriver.remove();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}