package com.google.Driver;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariOptions;

import com.google.framework.ConfigProvider;
import com.google.framework.ExReporter;
import com.google.framework.ProjectConfig;
import com.google.framework.TestData;
import com.relevantcodes.extentreports.LogStatus;

public class BrowserStackDriver {

	public static final String USERNAME = ProjectConfig.getPropertyValue("BSUserName");
	public static final String AUTOMATE_KEY = ProjectConfig.getPropertyValue("BSKey");
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

	static WebDriver remoteDriver = null;

	public static WebDriver getBSDriver() {
		remoteDriver = getRemoteDriver();
		return remoteDriver;
	}
public static WebDriver getcurrentDriver() {
	return remoteDriver;
}
	public static WebDriver getRemoteDriver() {
		// int trycount = 0;

		// String SelHost = ProjectConfig.getPropertyValue("SelHost");
		DesiredCapabilities desiredCap = new DesiredCapabilities();
		desiredCap.setCapability("browserstack.local", true);
		desiredCap.setCapability("browserstack.autoWait", 0);
		desiredCap.setCapability("build", ProjectConfig.getPropertyValue("buildNumber"));
		desiredCap.setCapability("project", "SCB");
		desiredCap.setCapability("browserstack.localIdentifier", "Test123");
		// String version = ConfigProvider.getConfig("Version");
		// String versionspec =
		// ProjectConfig.getPropertyValue("versionspecific");
		// String dev = ConfigProvider.getConfig("Device");
		String browsr = ConfigProvider.getConfig("Browser");
		String os = ConfigProvider.getConfig("OS");
		String versn = ConfigProvider.getConfig("Version");
		//String device = TestData.getConfig("devicename");

		try {
			switch (browsr.toUpperCase()) {
			case "ios":
				//System.out.println("Iphone Browser");
				desiredCap.setCapability("platform", "IOS");
				desiredCap.setCapability("device", TestData.getConfig("devicename"));
				desiredCap.setCapability("os", "IOS");
				//desiredCap.setCapability("device", "iPhone 7");
				//desiredCap.setCapability("realMobile", "true");
				desiredCap.setCapability("browser", "iPhone");
				//desiredCap.setCapability("browserstack.appium_version", "1.6.3");
				//desiredCap.setCapability("realMobile", true);
				break;
			case "IE":
				//System.out.println("Internet Explorer");
				desiredCap.setCapability("os", os);
				desiredCap.setCapability("browser", "IE");
				desiredCap.setCapability("browser_version", versn);
				break;
			case "FIREFOX":
				System.out.println("Firefox Browser");
				desiredCap.setCapability("os", "Windows");
				//desiredCap.setCapability("browserstack.selenium_version" , "3.3.0");
				System.out.println("Entering");
				FirefoxProfile profile = new FirefoxProfile();
				profile.setPreference("plugin.state.flash", 0);
				desiredCap.setCapability(FirefoxDriver.PROFILE, profile);
				//desiredCap.setBrowserName("firefox");
				desiredCap.setCapability("browser", "Firefox");
				//desiredCap.setCapability("browser_version", versn);
				break;
			case "CHROME":
				//ChromeOptions options = new ChromeOptions();
				desiredCap.setCapability("browser", "Chrome");
				desiredCap.setCapability("browser_version", "62.0");
				desiredCap.setCapability("os", "Windows");
				desiredCap.setCapability("os_version", "10");
				desiredCap.setCapability("resolution", "1024x768");
				desiredCap.setCapability("name", "Bstack-[Java] SCB Test");
				/*options.setExperimentalOption("excludeSwitches", "disable-popup-blocking");
				desiredCap.setCapability(ChromeOptions.CAPABILITY, options);*/
				break;
			case "SAFARI":
				System.out.println("Safari Browser");
			    desiredCap.setCapability("platform", "MAC");
				//desiredCap.setCapability("os", os);
				desiredCap.setCapability("browser", "Safari");
				desiredCap.setCapability("browser_version", versn);
				SafariOptions safarioptions = new SafariOptions();
				safarioptions.setUseTechnologyPreview(true);
				desiredCap.setCapability(SafariOptions.CAPABILITY, safarioptions);
				break;
			case "Android":
				//System.out.println("Android Browser");
				desiredCap.setCapability("platform", "ANDROID");
				//desiredCap.setCapability("browserName", "Chrome");
				desiredCap.setCapability("device", TestData.getConfig("devicename"));
				//desiredCap.setCapability("platform ","win");
				desiredCap.setCapability("os", "Android");
				desiredCap.setCapability("browser", "Android");
				//desiredCap.setCapability("os_version", "6.0"); for Samsung Galaxy Note 4 and "7.1" for Google Pixel.
				//desiredCap.setCapability("browserstack.appium_version", "1.4.16");
				
				break;

			case "desktop":
				//System.out.println(" Browser");
				desiredCap.setCapability("os", os);
				desiredCap.setCapability("browser", TestData.getConfig("BrowserName"));
				desiredCap.setCapability("browser_version", versn);
				break;
			default:
				desiredCap = DesiredCapabilities.firefox();
				break;
			}
			System.out.println("Before Remote");
			remoteDriver = new RemoteWebDriver(new URL(URL), desiredCap);
			System.out.println("After Remote"+(new URL(URL)));
			/*if (!(browsr.equals("Android")))
				remoteDriver.manage().window().maximize(); */
			ExReporter.log(LogStatus.INFO, "Browser Initiated successfully");
			remoteDriver.get(ProjectConfig.getPropertyValue("url"));
			remoteDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			//Thread.sleep(2000);

		} catch (Exception e) {
			try {
				Thread.sleep(2000);
				remoteDriver = new RemoteWebDriver(new URL(URL), desiredCap);
				//remoteDriver.manage().window().maximize();
				remoteDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			} catch (Exception e1) {
				System.out.println(e1.getMessage());
				ExReporter.log(LogStatus.FATAL, "Browser Initiation Failed :" + e1.getMessage());
				
			}
		}
		return remoteDriver;
	}
}