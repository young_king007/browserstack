package com.google.Driver;

import org.openqa.selenium.WebDriver;

public interface NewDriver {
	WebDriver getNewDriver();
}
