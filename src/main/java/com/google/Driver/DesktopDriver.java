package com.google.Driver;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.google.framework.ConfigProvider;
import com.google.framework.ExReporter;
import com.google.framework.ProjectConfig;
import com.relevantcodes.extentreports.LogStatus;


public class DesktopDriver implements NewDriver {

	WebDriver remoteDriver = null;

	@Override
	public WebDriver getNewDriver() {
		remoteDriver = getRemoteDriver();
		return remoteDriver;
	}

	public WebDriver getRemoteDriver() {
		//int trycount = 0;

		String SelHost = ProjectConfig.getPropertyValue("SelHost");
		DesiredCapabilities desiredCap = null;
		String OS=ConfigProvider.getConfig("OS");
		String version = ConfigProvider.getConfig("Version");
		String versionspec = ProjectConfig.getPropertyValue("versionspecific");

		System.out.println(ConfigProvider.getConfig("Browser"));

		try {
			switch (ConfigProvider.getConfig("Browser").toUpperCase()) {
			case "FIREFOX":
                desiredCap = DesiredCapabilities.firefox();
                desiredCap.setCapability("marionette", true);
                desiredCap.setCapability("acceptInsecureCerts", true);
                desiredCap.setCapability("proxyType", "proxy");

                if (versionspec.equals("true"))

                       desiredCap.setVersion(version);

                break;
				
			case "CHROME":
				if(OS.equalsIgnoreCase("MAC")){ 
					System.setProperty("webdriver.chrome.driver","./chromedriver");
					desiredCap = DesiredCapabilities.chrome();
					ChromeOptions options = new ChromeOptions();
					desiredCap.setBrowserName("chrome");
					desiredCap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
					
					if (versionspec.equals("true"))
						desiredCap.setVersion(version);
					
					Map<String, Object> prefs = new HashMap<>();

					// with this chrome still asks for permission
					prefs.put("profile.managed_default_content_settings.media_stream", 1);
					prefs.put("profile.managed_default_content_settings.media_stream_camera", 1);
					prefs.put("profile.managed_default_content_settings.media_stream_mic", 1);

					// and this prevents chrome from starting
					prefs.put("profile.content_settings.exceptions.media_stream_mic.https://*,*.setting", 1);
					prefs.put("profile.content_settings.exceptions.media_stream_mic.https://*,*.last_used", 1);
					prefs.put("profile.content_settings.exceptions.media_stream_camera.https://*,*.setting", 1);
					prefs.put("profile.content_settings.exceptions.media_stream_camera.https://*,*.last_used", 1);

					// and this prevents chrome from starting as well
					prefs.put("profile.content_settings.pattern_pairs.https://*,*.media_stream.video", "Allow");
					prefs.put("profile.content_settings.pattern_pairs.https://*,*.media_stream.audio", "Allow");

					options.setExperimentalOption("prefs", prefs);
					desiredCap.setCapability(ChromeOptions.CAPABILITY, options);
					
			
					break;
				}

				else{
					//System.setProperty("webdriver.chrome.driver","C:/Users/pandit/Documents/Pandi/chromedriver.exe");
					System.setProperty("webdriver.chrome.driver",".\\chromedriver.exe");
					System.out.println("chrome is going to started:::");

					ChromeOptions options = new ChromeOptions();
					options.addArguments("--start-maximized");
					options.addArguments("--disable-web-security");
					options.addArguments("--no-proxy-server");
					Map<String, Object> prefs = new HashMap<String, Object>();
					prefs.put("credentials_enable_service", false);
					prefs.put("profile.password_manager_enabled", false);
					options.setExperimentalOption("prefs", prefs);

					desiredCap = DesiredCapabilities.chrome();
					desiredCap.setBrowserName("chrome");
					if (versionspec.equals("true"))
						desiredCap.setVersion(version);
					
					Map<String, Object> prefs1 = new HashMap<>();

					// with this chrome still asks for permission
					prefs1.put("profile.managed_default_content_settings.media_stream", 1);
					prefs1.put("profile.managed_default_content_settings.media_stream_camera", 1);
					prefs1.put("profile.managed_default_content_settings.media_stream_mic", 1);

					// and this prevents chrome from starting
					prefs1.put("profile.content_settings.exceptions.media_stream_mic.https://*,*.setting", 1);
					prefs1.put("profile.content_settings.exceptions.media_stream_mic.https://*,*.last_used", 1);
					prefs1.put("profile.content_settings.exceptions.media_stream_camera.https://*,*.setting", 1);
					prefs1.put("profile.content_settings.exceptions.media_stream_camera.https://*,*.last_used", 1);

					// and this prevents chrome from starting as well
					prefs1.put("profile.content_settings.pattern_pairs.https://*,*.media_stream.video", "Allow");
					prefs1.put("profile.content_settings.pattern_pairs.https://*,*.media_stream.audio", "Allow");

					options.setExperimentalOption("prefs", prefs1);
					desiredCap.setCapability(ChromeOptions.CAPABILITY, options);
					
					break;
				}
			case "IE":
				
				System.setProperty("webdriver.ie.driver",".\\IEDriverServer.exe");
				System.out.println("IE is going to started:::");

				InternetExplorerOptions options = new InternetExplorerOptions();
				//options.
				/*options.addArguments("--start-maximized");
				options.addArguments("--disable-web-security");
				options.addArguments("--no-proxy-server");*/
				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("credentials_enable_service", false);
				prefs.put("profile.password_manager_enabled", false);
				//options.setExperimentalOption("prefs", prefs);

				desiredCap = DesiredCapabilities.chrome();
				desiredCap.setBrowserName("internet explorer");
				if (versionspec.equals("true"))
					desiredCap.setVersion(version);
				
				Map<String, Object> prefs1 = new HashMap<>();

				// with this chrome still asks for permission
				prefs1.put("profile.managed_default_content_settings.media_stream", 1);
				prefs1.put("profile.managed_default_content_settings.media_stream_camera", 1);
				prefs1.put("profile.managed_default_content_settings.media_stream_mic", 1);

				// and this prevents chrome from starting
				prefs1.put("profile.content_settings.exceptions.media_stream_mic.https://*,*.setting", 1);
				prefs1.put("profile.content_settings.exceptions.media_stream_mic.https://*,*.last_used", 1);
				prefs1.put("profile.content_settings.exceptions.media_stream_camera.https://*,*.setting", 1);
				prefs1.put("profile.content_settings.exceptions.media_stream_camera.https://*,*.last_used", 1);

				// and this prevents chrome from starting as well
				prefs1.put("profile.content_settings.pattern_pairs.https://*,*.media_stream.video", "Allow");
				prefs1.put("profile.content_settings.pattern_pairs.https://*,*.media_stream.audio", "Allow");

				//options.setExperimentalOption("prefs", prefs1);
				//desiredCap.setCapability(InternetExplorerOptions.CAPABILITY, options);
			
/*System.out.println("IE driver");
					 System.setProperty("webdriver.ie.driver",".\\IEDriverServer.exe");

				desiredCap = DesiredCapabilities.internetExplorer();
				desiredCap.setBrowserName("internet explorer");
				desiredCap.setCapability(InternetExplorerDriver.BROWSER_ATTACH_TIMEOUT, 30000);
				desiredCap.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
				desiredCap.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
				desiredCap.setCapability(InternetExplorerDriver.IE_SWITCHES, "-private");
				desiredCap.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
				if (versionspec.equals("true"))
					desiredCap.setVersion(version);*/
				break;
			case "SAFARI":
				//desiredCap = DesiredCapabilities.safari();
				//desiredCap.setCapability(SafariOptions.CAPABILITY, new SafariOptions());
				desiredCap.setBrowserName("safari");
				desiredCap.setCapability("webdriver.safari.noinstall", "false");
				desiredCap.setCapability("ACCEPT_SSL_CERTS", "true");
				break;
			/*case "EDGE":
				desiredCap = DesiredCapabilities.edge();
				//remoteDriver= new EdgeDriver(desiredCap);
				EdgeOptions options1 = new EdgeOptions();
				desiredCap.setCapability(options1.CAPABILITY, options1);
				desiredCap.setCapability(EdgeDriverService.EDGE_DRIVER_EXE_PROPERTY, true);
				desiredCap.setCapability("ACCEPT_SSL_CERTS", true);
				System.out.println("MICROSOFTEDGE Browser Launched");
				break;*/
			default:
				desiredCap = DesiredCapabilities.firefox();
				break;
			}
			System.out.println("Host "+SelHost);
			System.out.println("Desired cap  "+desiredCap);
			remoteDriver = new RemoteWebDriver(new URL(SelHost), desiredCap);
			remoteDriver.manage().deleteAllCookies();
			remoteDriver.manage().window().maximize();
			ExReporter.log(LogStatus.INFO, "Browser Initiated successfully");
			remoteDriver.get(ProjectConfig.getPropertyValue("url"));
			remoteDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		} catch (Exception e) {
			e.printStackTrace();
			ExReporter.log(LogStatus.FATAL, "Browser Initiation Failed :" + e.getMessage());
		}
		return remoteDriver;
	}
}