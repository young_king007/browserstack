package com.google.alltestpack;

import java.util.Map;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.google.library.teststeps.GoogleTestSteps;

public class GoogleTests extends GoogleMethods{
	
	/**
	 * Compatibility Test Cases
	 * @param brow
	 * @param data
	 * @param ctx
	 * @throws InterruptedException
	 */

	@Test(description = "Google - Layout Tests", dataProvider = "TestDataParallel")
	public static void CompatabilityFlow(Map<String, String> brow, Map<String, String> data, ITestContext ctx)
			throws InterruptedException {
		GoogleTestSteps steps = new GoogleTestSteps();
		String page_name="CompatabilityFlow";
		steps.sampleGoogleCompatibility(page_name);
	}

}
